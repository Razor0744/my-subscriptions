package team.four.mys.data.database

interface FirebaseDatabase {

    fun getUID(): String

    fun synchronization()
}