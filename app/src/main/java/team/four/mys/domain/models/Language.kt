package team.four.mys.domain.models

data class Language(
    var icon: Int,
    var name: String,
)