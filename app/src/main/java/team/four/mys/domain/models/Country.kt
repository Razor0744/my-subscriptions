package team.four.mys.domain.models

data class Country(
    var icon: Int,
    var name: String,
    var number: String
)
