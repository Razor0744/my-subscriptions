package team.four.mys.domain.models

import android.app.Activity

data class SetNavigationColorParam(
    val activity: Activity,
    val color: Int
)
